package com.awm.productbase.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.awm.productbase.entity.Product;
import com.awm.productbase.repository.ProductRepository;
import com.awm.productbase.service.ProductService;

@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	private ProductRepository productRepository;

	public List<Product> getAllProduct() {
		return productRepository.findAll();
	}

	public Product getProductByName(String name) {
		return productRepository.findByName(name);
	}

	public Product getProductById(int id) {
		return productRepository.findById(id).orElse(null);
	}

	public List<Product> saveAllProduct(List<Product> allProduct) {
		return productRepository.saveAll(allProduct);
	}

	public String deleteByIdProduct(int id) {
		productRepository.deleteById(id);
		return "id Removed" + id;
	}

	public Product updateProduct(Product product) { 
		  Product existingProduct =productRepository.findById(product.getId()).orElse(null);
	  existingProduct.setName(product.getName()); 
	  existingProduct.setPrice(product.getPrice()); 
	  existingProduct.setQuantity(product.getQuantity());
	  
	  
	  return productRepository.save(existingProduct); }

}
