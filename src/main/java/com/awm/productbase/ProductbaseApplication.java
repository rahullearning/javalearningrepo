package com.awm.productbase;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProductbaseApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProductbaseApplication.class, args);
	}

}
