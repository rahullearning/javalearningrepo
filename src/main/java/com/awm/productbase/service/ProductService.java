package com.awm.productbase.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.awm.productbase.entity.Product;
import com.awm.productbase.repository.ProductRepository;


public interface ProductService {
  
	public List<Product> getAllProduct();
	
	public Product getProductByName(String name);
	
	public Product getProductById(int id);

	public List<Product> saveAllProduct(List<Product> allProduct);
	
	public Product updateProduct(Product product);
}
