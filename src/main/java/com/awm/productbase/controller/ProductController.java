package com.awm.productbase.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.awm.productbase.entity.Product;
import com.awm.productbase.serviceImpl.ProductServiceImpl;

@RestController
public class ProductController {

	@Autowired
	ProductServiceImpl productServiceimpl ;

	@RequestMapping(value = "/getAllProduct", method = RequestMethod.GET, produces = { "application/json",
			" application/xml" })
	// @GetMapping(value = "/getAllProduct", produces = "application/json")
	public List<Product> getAllProduct() {
		return productServiceimpl.getAllProduct();
	}

	@RequestMapping(value = "/getProductByName/{name}", method = RequestMethod.GET, produces = { "application/json",
			" application/xml" })
	// @GetMapping(value = "/getProductByName", produces = "application/json")
	public Product getProductByName(@PathVariable String name) {
		return productServiceimpl.getProductByName(name);
	}

	@RequestMapping(value = "/getProductById/{id}", method = RequestMethod.GET, produces = { "application/json",
			" application/xml" })
	// @GetMapping(value="/getProductById/{id}",produces = "application/json")
	public Product getProductById(@PathVariable int id) {
		return productServiceimpl.getProductById(id);
	}

	@RequestMapping(value = "/saveAllProduct", method = RequestMethod.POST, consumes = { "application/json",
			" application/xml" })
	// @GetMapping(value="/saveAllProduct",consumes = "application/json")
	public List<Product> saveAllProduct(@RequestBody List<Product> allProduct) {
		System.out.println("Hello");
		return productServiceimpl.saveAllProduct(allProduct);
	}

	@RequestMapping(value = "/deleteByIdProduct/{id}", method = RequestMethod.DELETE, consumes = { "application/json"," application/xml" })
	// @DeleteMapping(value="/deleteByIdProduct/{id}",consumes="application/json")
	public String deleteByIdProduct(@PathVariable int id) {
		return productServiceimpl.deleteByIdProduct(id);
	}

	@RequestMapping(value = "/updateProduct", method = RequestMethod.PATCH, consumes = { "application/json",
			" application/xml" })
	// @PatchMapping(value="/updateProduct",consumes="application/json")
	public Product updateProduct(@RequestBody Product product) {
		return productServiceimpl.updateProduct(product);
	}
}
